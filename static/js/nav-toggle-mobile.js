function classToggle() {
    const navs = document.querySelectorAll('.nav-li')
    
    navs.forEach(nav => nav.classList.toggle('nav-li-show-mobile'));
  }
  
  document.querySelector('#nav-toggle-mobile')
    .addEventListener('click', classToggle);